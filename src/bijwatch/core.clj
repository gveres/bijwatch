(ns bijwatch.core
  (:require [net.cgrand.enlive-html :as html]
            [clojure.edn :as edn]
            [clojurewerkz.quartzite.scheduler :as qs]
            )
  (:gen-class))

(defn extract-from-raw
  [pg-source] (let [shows (html/select pg-source [:table#example :td html/content])]
                (->> shows
                     (filter string?)
                     (partition 8)
                     (map #(list (first %) (second %) (nth % 5)))
                     (set)
                     )))

(defn fetch-url [url]
  (html/html-resource (java.net.URL. url)))

(defn extract-shows
  "Leeches an URL and runs the results through a transducer to return a set of unique shows"
  [url extractor-fn]
  (let [pg-source (fetch-url url)
        current-shows (extractor-fn pg-source)
        prev-shows (set (edn/read-string (slurp "prev-shows.edn")))
        diff-shows (clojure.set/difference current-shows prev-shows)
        ]
    (spit "prev-shows.edn" (prn-str current-shows))
    (if (empty? diff-shows)
      "No change"
      (sort (map str diff-shows)))
    ))

(defn start-babszinhaz-check
  []
  (extract-shows
    "https://budapestbabszinhaz.hu/jegyek-musor/jegyvasarlas/"
    extract-from-raw
    ))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println (start-babszinhaz-check
             )))
